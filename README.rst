1. ``git clone https://bitbucket.org/sahchos/test_products``
2. ``cd test_products && virtualenv -p python3 venv``
3. ``source venv/bin/activate``
4. ``pip install -r requirements/local.txt``
5. Rename ``env.example`` to ``.env`` and setup variables. IMPORTANT change DB settings in ``.env`` file.
6. ``python manage.py migrate``
7. ``python manage.py runserver``
8. http://127.0.0.1:8000

|
Or run it with Docker
=====================

1. ``git clone https://bitbucket.org/sahchos/test_products``
2. ``mv test_products/env.docker_example test_products/.env``
3. ``cd test_products/docker_files && sudo docker-compose up``
4. http://127.0.0.1:8000
