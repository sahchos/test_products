wheel==0.29.0
Django==1.10
django-environ==0.4.1
django-braces==1.11.0
django-allauth==0.31.0

# Python-PostgreSQL Database Adapter
psycopg2==2.7.1

# Forms
django-crispy-forms==1.6.1
