# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.ProductsListView.as_view(), name='list'),
    url(r'^(?P<slug>[^/]+)/$', views.ProductView.as_view(), name='detail'),
    url(r'^(?P<slug>[^/]+)/like/$', views.ProductLikeView.as_view(), name='like'),
]
