# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django import forms
from django.utils.translation import ugettext as _

from .models import Comment


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['text']
        widgets = {
            'text': forms.Textarea(attrs={
                'cols': 60,
                'rows': 5,
                'placeholder': _('Write your comment here'),
                'required': True
            })
        }
        labels = {'text': ''}
