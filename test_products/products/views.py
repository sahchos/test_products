# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from datetime import timedelta

from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import ListView, FormView, DetailView
from django.views.generic.base import View
from django.db.models import Prefetch
from django.contrib import messages
from django.utils.translation import ugettext as _
from django.shortcuts import redirect, get_object_or_404
from django.db.models import Count
from django.utils import timezone
from braces.views import LoginRequiredMixin

from .models import Product, Comment
from .forms import CommentForm


class ProductsListView(ListView):
    model = Product
    template_name = 'products/list.html'
    context_object_name = 'products'
    paginate_by = settings.PRODUCT_PER_PAGE

    def get_queryset(self):
        order = 'pk'
        if self.request.GET.get('sort_direction') in ['asc', 'desc']:
            order = '-likes_num' if self.request.GET['sort_direction'] == 'asc' else 'likes_num'

        queryset = Product.objects.annotate(likes_num=Count('likes')).order_by(order)
        return queryset


class ProductView(FormView, DetailView):
    model = Product
    form_class = CommentForm
    template_name = 'products/product.html'
    context_object_name = 'product'

    def get_success_url(self):
        return reverse('products:detail', kwargs={'slug': self.kwargs['slug']})

    def form_valid(self, form):
        form.instance.product = self.get_object()
        if self.request.user.is_authenticated:
            form.instance.created_by = self.request.user
        form.save()

        messages.add_message(self.request, messages.SUCCESS, _('Your comment successfully added'))

        return super(ProductView, self).form_valid(form)

    def get_queryset(self):
        time_threshold = timezone.now() - timedelta(hours=settings.COMMENTS_TIME_THRESHOLD)
        queryset = Product.objects.filter(slug=self.kwargs['slug']).prefetch_related(
            'likes',
            Prefetch('comments', queryset=Comment.objects.filter(
                created_at__gt=time_threshold
            ).select_related('created_by').order_by('-created_at')),
        )

        return queryset


class ProductLikeView(LoginRequiredMixin, View):

    def post(self, request, **kwargs):
        product = get_object_or_404(Product, slug=kwargs['slug'])

        user_liked = request.user.product_likes.filter(slug=kwargs['slug']).exists()
        if user_liked:
            messages.add_message(request, messages.INFO, _('You already liked this product'))
        else:
            product.likes.add(request.user)
            messages.add_message(request, messages.SUCCESS, _('Your like successfully added'))

        return redirect('products:detail', kwargs['slug'])
