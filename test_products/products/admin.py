# -*- coding: utf-8 -*-
from __future__ import absolute_import

from django.contrib import admin

from .models import Product, Comment

admin.site.register(Comment)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    def view_on_site(self, obj):
        return obj.get_absolute_url()
