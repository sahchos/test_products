from test_plus.test import TestCase

from ..models import Product, Comment


class ProductTest(TestCase):

    def setUp(self):
        self.user = self.make_user()
        self.product = Product.objects.create(
            name='Test product',
            slug='test-product',
            description='desc',
            created_by=self.user,
            price=123.11
        )

    def test__str__(self):
        self.assertEqual(self.product.__str__(), self.product.name)

    def test_get_absolute_url(self):
        self.assertEqual(self.product.get_absolute_url(), self.reverse('products:detail', slug=self.product.slug))


class CommentTest(TestCase):

    def test__str__(self):
        user = self.make_user()
        product = Product.objects.create(
            name='Test product',
            slug='test-product',
            description='desc',
            created_by=user,
            price=123.11
        )

        self.comment = Comment.objects.create(
            created_by=user,
            product=product,
            text='Test comment for Test product ')
        self.assertEqual(self.comment.__str__(), 'Comment Test comment for Tes')
