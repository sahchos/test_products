from test_plus.test import CBVTestCase, TestCase

from datetime import timedelta

from django.conf import settings
from django.test.client import RequestFactory
from django.utils import timezone

from ..models import Product, Comment
from ...products import views


class ProductsListViewTest(TestCase):

    def setUp(self):
        self.user = self.make_user()
        self.user2 = self.make_user('user2')
        self.user3 = self.make_user('user3')
        self.factory = RequestFactory()

        Product.objects.bulk_create(
            [Product(
                name='{} product'.format(i),
                slug='slug_{}'.format(i),
                price=1,
                created_by=self.user,
            ) for i in range(20)]
        )

        products = Product.objects.all()
        for i, product in enumerate(products):
            if i == len(products) - 1:
                product.likes = []
            elif i == 1:
                product.likes = [self.user, self.user2, self.user3]
            else:
                product.likes = [self.user]

    def test_get_queryset(self):
        view = CBVTestCase().get_instance(views.ProductsListView)
        view.request = self.factory.get(self.reverse('products:list'))

        with self.assertNumQueriesLessThan(2):
            res = view.get_queryset()

            self.assertEqual(len(res), 20)
            self.assertEqual(res[1].likes_num, 3)
            self.assertGreater(res[19].pk, res[0].pk)

    def test_order_by_likes(self):
        # fake param sort by pk
        response = self.get(self.reverse('products:list'), data={'sort_direction': 'fake'})
        self.assertEqual(len(response.context['products']), settings.PRODUCT_PER_PAGE)
        self.assertEqual(response.context['products'][1].likes_num, 3)

        response = self.get(self.reverse('products:list'), data={'sort_direction': 'asc'})
        self.assertEqual(len(response.context['products']), settings.PRODUCT_PER_PAGE)
        self.assertGreater(
            response.context['products'][0].likes_num,
            response.context['products'][settings.PRODUCT_PER_PAGE - 1].likes_num
        )

        response = self.get(self.reverse('products:list'), data={'sort_direction': 'desc'})
        self.assertEqual(len(response.context['products']), settings.PRODUCT_PER_PAGE)
        self.assertGreater(
            response.context['products'][1].likes_num,
            response.context['products'][0].likes_num
        )


class ProductViewTest(TestCase):

    def setUp(self):
        self.user = self.make_user()
        self.user2 = self.make_user('user2')
        self.product = Product.objects.create(
            name='Test product',
            slug='slug_test',
            price=1,
            created_by=self.user
        )
        self.product.likes = [self.user, self.user2]
        self.cbv_view = CBVTestCase().get_instance(views.ProductView, slug=self.product.slug)

    def test_get_success_url(self):
        self.assertEqual(self.cbv_view.get_success_url(), self.reverse('products:detail', slug=self.product.slug))

    def test_form_valid(self):
        self.assertEqual(Comment.objects.count(), 0)

        with self.login(self.user):
            response = self.post(
                self.reverse('products:detail', slug=self.product.slug),
                data={'text': 'New comment'},
            )

        self.assertRedirects(response, self.product.get_absolute_url())
        self.assertEqual(Comment.objects.count(), 1)
        self.assertEqual(Comment.objects.first().created_by_id, self.user.pk)

    def test_get_queryset(self):
        # add comments
        created_at = timezone.now() - timedelta(hours=25)
        self.comment = Comment.objects.create(text='Comment 1', product=self.product)
        self.comment2 = Comment.objects.create(text='Comment 2', product=self.product)
        self.comment3 = Comment.objects.create(text='Comment 3', product=self.product)

        self.comment3.created_at = created_at
        self.comment3.save()

        with self.assertNumQueriesLessThan(4):
            res = self.cbv_view.get_queryset()

            self.assertEqual(len(res), 1)
            self.assertEqual(res[0].slug, self.product.slug)
            self.assertEqual(len(res[0].comments.all()), 2)
            self.assertEqual(len(res[0].likes.all()), 2)
            self.assertGreater(res[0].comments.all()[0].created_at, res[0].comments.all()[1].created_at)


class ProductLikeViewTest(TestCase):

    def setUp(self):
        self.user = self.make_user()
        self.product = Product.objects.create(
            name='Test product',
            slug='slug_test',
            price=1,
            created_by=self.user
        )

    def test_login_required(self):
        self.assertLoginRequired('products:like', slug=self.product.slug)

    def test_product_like(self):
        self.assertEqual(self.product.likes.count(), 0)

        with self.login(self.user):
            response = self.post(self.reverse('products:like', slug=self.product.slug))
            self.assertRedirects(response, self.reverse('products:detail', slug=self.product.slug))
            self.assertEqual(self.product.likes.count(), 1)

            # second attempt to like will not add it
            response = self.post(self.reverse('products:like', slug=self.product.slug))
            self.assertRedirects(response, self.reverse('products:detail', slug=self.product.slug))
            self.assertEqual(self.product.likes.count(), 1)
