from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY')
ALLOWED_HOSTS = []
EMAIL_BACKEND = 'core.mail.backends.smtp.EmailBackend'

PRODUCT_PER_PAGE = 50
COMMENTS_TIME_THRESHOLD = 24

# other production specific settings
